from django.shortcuts import render

# Create your views here.
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def testClientSSO(request):
    json_data = {'name': 'nalanxiao', 'id': 0}
    return JsonResponse(json_data)

@login_required
def zheng(request):
    json_data = {'zheng':'test_zheng'}
    return JsonResponse(json_data)