from django.db import models

 class Payment(BaseModel):
        """
        支付信息
        """
        order_id = models.CharField(
            max_length=100, verbose_name="订单号")
        trade_id = models.CharField(
            max_length=100, verbose_name="支付流水号")

    class Meta:
        db_table = 'tb_payment'
        verbose_name = '支付信息'
        verbose_name_plural = verbose_name