### Django调用微信推送接口

源码：**[Gitee地址](https://gitee.com/jingluoonline/Django-Extended-App.git)**

**欢迎Star~**

#### 一、微信授权登录（OAuth2.0）

​		**OAuth**（开放授权）是一个开放标准，允许用户让第三方应用访问该用户在某一网站上存储的私密的资源（如照片，视频，联系人列表），而无需将用户名和密码提供给第三方应用。具体详情可以参考[阮一峰blog](http://www.ruanyifeng.com/blog/2019/04/oauth_design.html)

​		参考文档：[微信公众平台开发者文档](https://developers.weixin.qq.com/doc/offiaccount/Getting_Started/Overview.html)

#### 二、申请公众号测试账号

[申请地址](https://mp.weixin.qq.com/debug/cgi-bin/sandbox?t=sandbox/login)

1. **获取`appID`和`appsecret`**

2. 下拉，扫描**测试号二维码**，关注后右侧会出现用户列表

3. 记录出现的微信号，即为用户的**`openid`**

4. 自定义**消息模板接口**

5. 如果有自己的域名资源，可以去设置**域名配置**，去获取网页授权。**注意端口：443/80**

6. 网页服务—>网页账号—>[网页授权获取用户基本信息](https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/Wechat_webpage_authorization.html)—>修改

#### 三、流程

1. 进入页面
2. 生成二维码
3. 调用回调，获取扫码用户`openid`
4. 推送消息

#### 四、使用方式

1. `python manage.py makemigrations`
2. `python manage.py migrate`
3. 修改`settings.py`下`app_id  appsecret  redirect_uri  template_id`
4. `python manage.py runserver 0.0.0.0:8001`
5. 输入网址`http://127.0.0.1:8001/index/`

#### 五、功能实现

##### 1. 生成二维码

**目的：通过网页授权，获取code**

```python
def bind_qcode(request):
    """
    生成二维码
    :param request: 
    :return: 
    """
    ret = {'code': 1000}
    user = str(models.UserInfo.objects.get(id=1).id)
    try:
        m=hashlib.md5()
        m.update(user.encode('utf-8'))
        access_url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={appid}&redirect_uri={redirect_uri}&response_type=code&scope=snsapi_userinfo&state={state}#wechat_redirect"
        access_url = access_url.format(
            # 商户的appid
            appid=settings.WECHAT_CONFIG["app_id"], # 'wx6edde7a6a97e4fcd',
            # 回调地址
            redirect_uri=settings.WECHAT_CONFIG["redirect_uri"],
            # 当前登录用户的唯一id
            state=m.hexdigest # 为当前用户生成MD5值
        )
        ret['data'] = access_url
    except Exception as e:
        ret['code'] = 1001
        ret['msg'] = '失败'
        ret['data'] = ''
    return JsonResponse(ret)
```

##### 2. 回调函数

**目的：获取扫码用户的唯一ID**

```python
def callback(request):
    """
    用户在手机微信上扫码后，微信自动调用该方法。
    用于获取扫码用户的唯一ID，以后用于给他推送消息。
    """
    code = request.GET.get("code")
    # 用户md5值,用户唯一id
    state = request.GET.get("state")
    # 获取该用户openId(用户唯一，用于给用户发送消息)
    # request模块朝https://api.weixin.qq.com/sns/oauth2/access_token地址发get请求
    res = requests.get(
        url="https://api.weixin.qq.com/sns/oauth2/access_token",
        params={
            "appid": settings.WECHAT_CONFIG['app_id'],
            "secret": settings.WECHAT_CONFIG['appsecret'],
            "code": code,
            "grant_type": 'authorization_code',
        }
    ).json()
    # res.data   是json格式
    # res=json.loads(res.data)
    # res是一个字典
    # 获取的到openid表示用户授权成功
    openid = res.get("openid")
    if openid:
        models.UserInfo.objects.filter(uid=state).update(wx_id=openid)
        response = "<h1>授权成功 %s </h1>" % openid
    else:
        response = "<h1>用户扫码之后，手机上的提示</h1>"
    return HttpResponse(response)
```

##### 3. 发送信息

**目的：定向给用户发送信息**

```python
def sendmsg(request):
    # 获取access_token，默认有效期2小时
    def get_access_token():
        result = requests.get(
            url="https://api.weixin.qq.com/cgi-bin/token",
            params={
                "grant_type": "client_credential",
                "appid": settings.WECHAT_CONFIG['app_id'],
                "secret": settings.WECHAT_CONFIG['appsecret'],
            }
        ).json()
        if result.get("access_token"):
            access_token = result.get('access_token')
        else:
            access_token = None
        return access_token
    access_token = get_access_token()
    openid = models.UserInfo.objects.get(id=1).wx_id

	# 发送模板消息
    def send_template_msg():
        res = requests.post(
            url="https://api.weixin.qq.com/cgi-bin/message/template/send",
            params={
                'access_token': access_token
            },
            json={
                "touser": openid,
                "template_id": settings.WECHAT_CONFIG['template_id'],
            }
        )
        result = res.json()
        return result
    ret = {'code': 1000}
    result = send_template_msg()
    if result.get('errcode') == 0:
        ret['msg'] = '发送成功'
        return JsonResponse(ret)
    return JsonResponse(ret)
```

