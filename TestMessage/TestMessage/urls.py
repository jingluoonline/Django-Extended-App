from django.contrib import admin
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from demo import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path(r'index/', views.index),
    path(r'bind_qcode/', views.bind_qcode),
    path(r'callback/', views.callback),
    path(r'sendmsg/', views.sendmsg),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
