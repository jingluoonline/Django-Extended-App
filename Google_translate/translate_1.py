import xlrd
from xlutils import copy
from google_trans_new import google_translator

def translate_base(text, lang_tgt):
	t = google_translator(timeout=20)
	translate_text = t.translate(text, lang_tgt)
	print(translate_text)
	return translate_text

excel_file = "本地化语言.xls" # 设置excel文件名
file = xlrd.open_workbook(excel_file) # 读取excel文件
write_file = copy.copy(file)
write_sheet = write_file.get_sheet(0)
sheet = file.sheet_by_index(0) #获取所有的sheet页
sheet_rows = sheet.nrows # 获取当前sheet行数
sheet_cols = sheet.ncols # 获取当前sheet列数
for sheet_row in range(1, sheet_rows):
	sheet_value = str(sheet.cell(sheet_row,0).value)
	write_sheet.write(sheet_row, 1, translate_base(sheet_value, 'en'))
	write_sheet.write(sheet_row, 2, translate_base(sheet_value, 'pl'))
	write_sheet.write(sheet_row, 3, translate_base(sheet_value, 'de'))
	write_sheet.write(sheet_row, 4, translate_base(sheet_value, 'ru'))
	write_sheet.write(sheet_row, 5, translate_base(sheet_value, 'fr'))
	write_sheet.write(sheet_row, 6, translate_base(sheet_value, 'ko'))
	write_sheet.write(sheet_row, 7, translate_base(sheet_value, 'nl'))
	write_sheet.write(sheet_row, 8, translate_base(sheet_value, 'ms'))
	write_sheet.write(sheet_row, 9, translate_base(sheet_value, 'no'))
	write_sheet.write(sheet_row, 10, translate_base(sheet_value, 'pt'))
	write_sheet.write(sheet_row, 11, translate_base(sheet_value, 'ja'))
	write_sheet.write(sheet_row, 12, translate_base(sheet_value, 'sv'))
	write_sheet.write(sheet_row, 13, translate_base(sheet_value, 'th'))
	write_sheet.write(sheet_row, 14, translate_base(sheet_value, 'tr'))
	write_sheet.write(sheet_row, 15, translate_base(sheet_value, 'es'))
	write_sheet.write(sheet_row, 16, translate_base(sheet_value, 'el'))
	write_sheet.write(sheet_row, 17, translate_base(sheet_value, 'it'))
	write_sheet.write(sheet_row, 18, translate_base(sheet_value, 'id'))
	write_sheet.write(sheet_row, 19, translate_base(sheet_value, 'vi'))
	write_sheet.write(sheet_row, 20, translate_base(sheet_value, 'zh-hk'))
write_file.save(excel_file)

# pip install xlrd==1.2.0
# pip install google_trans_new
# pip install xlutils